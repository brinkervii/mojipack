import mojipack
from mojipack import DEFAULT_BYTE_MAPPING


def test_default_codec_functions_e2e(some_test_message: bytes):
    for _ in range(10_000):
        encoded = mojipack.encode(some_test_message)

        try:
            decoded = mojipack.decode(encoded)

            assert decoded == some_test_message

        except Exception as e:
            print(encoded)
            raise e


def test_ensure_every_emoji_works(some_test_message: bytes):
    for emoji in DEFAULT_BYTE_MAPPING.values():
        message = ""

        while emoji not in message:
            message = mojipack.encode(some_test_message)
            _ = mojipack.decode(message)

        print(message)
        print("")
